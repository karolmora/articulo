<?php
require_once "persistencia/conexion.php";
require_once "persistencia/ArticuloDAO.php";
class Articulo{
    private $idArticulo;
    private $titulo;
    private $descripcion;
    private $fecha;
    private $conexion;
    private $ArticuloDAO;

    public function getId(){
        return $this -> id;
    }

    public function getTitulo(){
        return $this -> titulo;
    }

    public function getDescripcion(){
        return $this -> descripcion;
    }

    public function getFecha(){
        return $this -> fecha;
    }

    public function Articulo($idArticulo = "", $titulo = "", $descripcion = "", $fecha = ""){
        $this -> id = $idArticulo;
        $this -> nombre = $titulo;
        $this -> apellido = $descripcion;
        $this -> correo = $fecha;
        $this -> conexion = new Conexion();
        $this -> clienteDAO = new ClienteDAO($this -> idArticulo, $this -> titulo, $this -> descripcion, $this -> fecha);
    }


    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticuloDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> idArticulo = $resultado[0];
        $this -> titulo = $resultado[1];
        $this -> descripcion = $resultado[2];
        $this -> fecha = $resultado[3];
    }

}

?>
